﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace csv轉換
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_source_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.ShowDialog();
            this.txt_sourcefile.Text = file.FileName;
        }

        private void btn_target_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            this.txt_target.Text = path.SelectedPath;
        }

        private void btn_open_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(txt_target.Text.ToString());
        }

        private void btn_format_Click(object sender, EventArgs e)
        {
            try
            {
                string filetarget = txt_target.Text+@"\" + DateTime.Now.ToString("yyyyMMdd HH-mm-ss")+".csv";
                using (StreamWriter csvStreamWriter = new StreamWriter(filetarget, false, System.Text.Encoding.GetEncoding("utf-8")))
                {
                    string rowdata = "";
                    rowdata += "幼兒身分證號,嬰幼兒姓名,嬰幼兒性別,幼兒出生日期,同胎次序,通訊地址,電話,父或母身分證號,接種機構,接種日期,疫苗種類,疫苗劑別,疫苗批號,疫苗廠商,疫苗型別,曾接種流感疫苗";
                    csvStreamWriter.WriteLine(rowdata);
                    string line;
                    using (System.IO.StreamReader file = new System.IO.StreamReader(txt_sourcefile.Text, System.Text.Encoding.GetEncoding("Big5")))
                    {
                        while ((line = file.ReadLine()) != null)
                        {
                            rowdata="";
                            string sex = "";
                            string line_r = line.Replace("\"","");
                            String[] substrings = line_r.Split(',');
                            rowdata += substrings[0] + ",";     //幼兒身分證號
                            rowdata += substrings[1] + ",";     //嬰幼兒姓名

                            if (substrings[2] == "M")
                            {
                                sex = "男";
                            }
                            else
                            {
                                sex = "女";
                            }

                            rowdata += sex + ",";     //嬰幼兒性別
                            rowdata += substrings[3] + ",";     //幼兒出生日期
                            rowdata += substrings[4] + ",";     //同胎次序
                            rowdata += substrings[5] + ",";     //通訊地址
                            rowdata += substrings[6] + ",";     //電話
                            rowdata += substrings[7] + ",";     //父或母身分證號
                            rowdata += substrings[8] + ",";     //接種機構
                            rowdata += substrings[9] + ",";     //接種日期
                            rowdata += substrings[10] + ",";    //疫苗種類
                            rowdata += substrings[11] + ",";    //疫苗劑別
                            rowdata += substrings[12] + ",";    //疫苗批號
                            rowdata += substrings[13] + ",";    //疫苗廠商
                            rowdata += substrings[14] + ",";    //疫苗型別
                            rowdata += substrings[15];          //曾接種流感疫苗
                            csvStreamWriter.WriteLine(rowdata);
                        }
                        
                    }

                }

                MessageBox.Show("轉換完成\t 檔案路徑：" + filetarget);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Exception:{0}", ex.Message.ToString()));
            }
        }
    }
}
