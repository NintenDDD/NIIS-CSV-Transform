﻿namespace csv轉換
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_sourcefile = new System.Windows.Forms.TextBox();
            this.btn_source = new System.Windows.Forms.Button();
            this.btn_target = new System.Windows.Forms.Button();
            this.txt_target = new System.Windows.Forms.TextBox();
            this.btn_open = new System.Windows.Forms.Button();
            this.btn_format = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_sourcefile
            // 
            this.txt_sourcefile.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txt_sourcefile.Location = new System.Drawing.Point(61, 37);
            this.txt_sourcefile.Name = "txt_sourcefile";
            this.txt_sourcefile.Size = new System.Drawing.Size(286, 30);
            this.txt_sourcefile.TabIndex = 0;
            // 
            // btn_source
            // 
            this.btn_source.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_source.Location = new System.Drawing.Point(353, 29);
            this.btn_source.Name = "btn_source";
            this.btn_source.Size = new System.Drawing.Size(115, 40);
            this.btn_source.TabIndex = 1;
            this.btn_source.Text = "檔案來源";
            this.btn_source.UseVisualStyleBackColor = true;
            this.btn_source.Click += new System.EventHandler(this.btn_source_Click);
            // 
            // btn_target
            // 
            this.btn_target.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_target.Location = new System.Drawing.Point(353, 95);
            this.btn_target.Name = "btn_target";
            this.btn_target.Size = new System.Drawing.Size(115, 37);
            this.btn_target.TabIndex = 2;
            this.btn_target.Text = "完成位置";
            this.btn_target.UseVisualStyleBackColor = true;
            this.btn_target.Click += new System.EventHandler(this.btn_target_Click);
            // 
            // txt_target
            // 
            this.txt_target.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txt_target.Location = new System.Drawing.Point(61, 101);
            this.txt_target.Name = "txt_target";
            this.txt_target.Size = new System.Drawing.Size(286, 30);
            this.txt_target.TabIndex = 0;
            this.txt_target.Text = "C:\\temp";
            // 
            // btn_open
            // 
            this.btn_open.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_open.Location = new System.Drawing.Point(488, 95);
            this.btn_open.Name = "btn_open";
            this.btn_open.Size = new System.Drawing.Size(119, 36);
            this.btn_open.TabIndex = 3;
            this.btn_open.Text = "開啟資料夾";
            this.btn_open.UseVisualStyleBackColor = true;
            this.btn_open.Click += new System.EventHandler(this.btn_open_Click);
            // 
            // btn_format
            // 
            this.btn_format.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_format.Location = new System.Drawing.Point(221, 180);
            this.btn_format.Name = "btn_format";
            this.btn_format.Size = new System.Drawing.Size(206, 50);
            this.btn_format.TabIndex = 4;
            this.btn_format.Text = "轉換格式";
            this.btn_format.UseVisualStyleBackColor = true;
            this.btn_format.Click += new System.EventHandler(this.btn_format_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 293);
            this.Controls.Add(this.btn_format);
            this.Controls.Add(this.btn_open);
            this.Controls.Add(this.btn_target);
            this.Controls.Add(this.btn_source);
            this.Controls.Add(this.txt_target);
            this.Controls.Add(this.txt_sourcefile);
            this.Name = "Form1";
            this.Text = "單機版NIIS格式轉換                最後修改:1070302";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_sourcefile;
        private System.Windows.Forms.Button btn_source;
        private System.Windows.Forms.Button btn_target;
        private System.Windows.Forms.TextBox txt_target;
        private System.Windows.Forms.Button btn_open;
        private System.Windows.Forms.Button btn_format;
    }
}

